import visa
import json
import time
import driver
import numpy as np
import argparse

description = ("Reads data from a LCR device")
parser = argparse.ArgumentParser(description=description)

parser.add_argument("-c", type=str, metavar="config.json",
                    help=("Json with the device configuration."),
                    action="store", required=True, 
                    dest="configuration")
parser.add_argument("-i", type=str, metavar="input.dat",
                    help=("File with the datapoints to meassure."),
                    action="store", required=True, 
                    dest="input")
parser.add_argument("-o", type=str, metavar="output.dat",
                    help=("Output file to place the meassurements"),
                    action="store", required=True,
                    dest="output")
args = parser.parse_args()

print("Loading Configuration")
configuration = json.load(open(args.configuration))
print("\tConfiguration Loaded")

print("Connecting to device")
device = driver.openVisaResource(
    configuration["device-config"]["address"]
)
print("\tDone")

print("Initializing instrument")
name = driver.initInstrument(
    device,
    do_reset=configuration["device-config"]["do-reset"]
)
if name == -1:
    raise RuntimeError("Error connecting to device")
print(f"Connected to device {name}")

print("Setting correction")
correction_parameters = configuration["correction-parameters"]
code = driver.setCorrectionParameters(
    device,
    correction_parameters["cable-length"],
    correction_parameters["correction-method"],
    correction_parameters["open-correction"],
    correction_parameters["short-correction"],
    correction_parameters["load-correction"],
    correction_parameters["load-type"],
    correction_parameters["correction-channel"]
)
if code == -1:
    raise RuntimeError("Error setting correction values")
print("\tDone")

print("Setting integration time")
meassure_config = configuration["integration-time"]
code = driver.setIntegrationTime(
    device,
    meassure_config["aperture-type"],
    meassure_config["averaging-rate"]
)
if code == -1:
    raise RuntimeError("Error setting integration time")
print("\tDone")

print("Setting impedance confgiuration")
impedance_config = configuration["impedance-config"]
code = driver.setImpedance(
    device,
    impedance_config["impedance-range"],
    impedance_config["impedance-auto-range"],
    impedance_config["impedance-type"]
)
if code == -1:
    raise RuntimeError("Error setting the impedance cofniguration")
print("\tDone")

print("Starting meassures")
data = np.loadtxt(args.input)
with open(args.output, "w") as file:
    for datum in data:
        print(f"Meassuring w={datum[0]}, v={datum[1]}: ", end=" ")
        code = driver.setSignalLevelAndFrequency(
            device,
            datum[0],
            datum[1],
            True,
            configuration["meassure-config"]["auto-level-control"]
        )
        if code == -1:
            text = f"Error setting the device to F={datum[0]} and V={datum[1]}"
            raise RuntimeError(text)
        time.sleep(
            max(configuration["meassure-config"]["meassure-timelapse"]/datum[0],
                configuration["meassure-config"]["minimum-time"])
        )
        read = driver.fetchData(device)
        if isinstance(read, int) and read == -1:
            raise RuntimeError("Error fetching the data")
        parsed = " ".join(f"{i}" for i in read) + "\n"
        print(parsed)
        parsed = f"{datum[0]} {datum[1]} " + parsed
        file.write(parsed)
        file.flush()
